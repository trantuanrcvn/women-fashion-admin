<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'auth', 'name' => 'auth'], function () {
    Route::post('login', 'Api\AuthController@login');
    Route::post('register', 'Api\AuthController@register');
    Route::post('refresh', 'Api\AuthController@refresh')->middleware('jwt.auth');
    Route::post('logout', 'Api\AuthController@logout')->middleware('jwt.auth');
});

Route::group(['middleware' => 'jwt.auth'], function () {

    //Group route admin
    Route::group(['prefix' => 'admin', 'name' => 'admin'], function () {
        Route::get('/', 'Api\AdminController@index')->name('index');
        Route::post('/', 'Api\AdminController@store')->name('store');
        Route::get('{id}', 'Api\AdminController@show')->name('show');
        Route::put('{id}', 'Api\AdminController@update')->name('update');
        Route::patch('{id}', 'Api\AdminController@update')->name('update');
        Route::delete('{id}', 'Api\AdminController@destroy')->name('destroy');
    });

    //Group route user
    Route::group(['prefix' => 'users'], function () {
        Route::get('/', 'Api\UserController@index');
        Route::post('/', 'Api\UserController@store');
        Route::get('{id}', 'Api\UserController@show');
        Route::put('{id}', 'Api\UserController@update');
        Route::patch('{id}', 'Api\UserController@update');
        Route::delete('{id}', 'Api\UserController@destroy');
    });
});
