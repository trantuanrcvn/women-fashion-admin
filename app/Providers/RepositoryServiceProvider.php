<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
        $this->app->singleton(
            'App\Repositories\Eloquent\User\UserRepositoryInterface',
            'App\Repositories\Eloquent\User\UserRepository');
        $this->app->singleton(
            'App\Repositories\Eloquent\Admin\AdminRepositoryInterface',
            'App\Repositories\Eloquent\Admin\AdminRepository');
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
