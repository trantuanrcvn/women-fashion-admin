<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\BaseApiController;
use App\Repositories\Eloquent\Admin\AdminRepositoryInterface;

class AdminController extends BaseApiController
{
    /**
     * AdminController constructor.
     * @param AdminRepositoryInterface $adminRepository
     */
    public function __construct(AdminRepositoryInterface $adminRepository)
    {
        parent::__construct($adminRepository);
    }
}
