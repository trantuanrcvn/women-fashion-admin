<?php

namespace App\Http\Controllers;

use App\Http\Resources\BaseApiResource;
use App\Http\Resources\BaseApiResourceCollection;
use App\Repositories\Eloquent\RepositoryInterface;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\ResourceCollection;

class BaseApiController extends Controller
{
    /**
     * Define repository
     * @var RepositoryInterface
     */
    protected $repository;

    /**
     * BaseApiController constructor.
     * @param RepositoryInterface $repository
     */
    public function __construct(RepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function index(Request $request)
    {
        $params = $request->all();
        if ($params === []) {
            $data = $this->repository->getAll();
        } else {
            $data = $this->repository->get($params);
        }

        $dataResource = new BaseApiResourceCollection($data);
        return $this->apiResponse200($dataResource);
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function store(Request $request)
    {
        $data = $this->repository->create($request->all());
        $dataResource = new BaseApiResource($data);
        return $this->apiResponse201($dataResource);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function show($id)
    {
        $data = $this->repository->find($id);
        if ($data === null) {
            return $this->apiResponse404();
        }
        $dataResource = new BaseApiResource($data);
        return $this->apiResponse200($dataResource);
    }

    /**
     * @param $id
     * @param Request $request
     * @return mixed
     */
    public function update($id, Request $request)
    {
        $data = $this->repository->update($id, $request->all());
        if ($data === null) {
            return $this->apiResponse404();
        }
        $dataResource = new BaseApiResource($data);
        return $this->apiResponse200($dataResource);
    }

    /**
     * @param $id
     * @return JsonResponse
     */
    public function destroy($id)
    {
        $flag = $this->repository->delete($id);
        if (!$flag) {
            return $this->apiResponse404();
        }
        return $this->apiResponse204();
    }

    /**
     * @param int $statusCode
     * @param array $data
     * @param array $header
     * @param string $message
     * @return JsonResponse
     */
    public function apiResponse(int $statusCode, $data = [], $header = [], $message = '')
    {
        return response()
            ->json([
                'data' => $data,
                'message' => $message,
            ], $statusCode)->withHeaders($header);
    }

    /**
     * @param $data
     * @param array $header
     * @return JsonResponse
     */
    public function apiResponse200($data, $header = [])
    {
        return $data->response()->setStatusCode(200)->withHeaders($header);
    }

    /**
     * @param $data
     * @param array $header
     * @return JsonResponse
     */
    public function apiResponse201($data, $header = [])
    {
        return $data->response()
            ->setStatusCode(201)->withHeaders($header);
    }

    /**
     * @param array $header
     * @return JsonResponse
     */
    public function apiResponse204($header = [])
    {
        return response()
            ->json([], 204)->withHeaders($header);
    }

    /**
     * @param array $header
     * @return JsonResponse
     */
    public function apiResponse404($header = [])
    {
        return response()
            ->json([
                'data' => null,
                'message' => 'Not found',
            ], 404)->withHeaders($header);
    }
}
