<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\ResourceCollection;

class BaseApiResourceCollection extends ResourceCollection
{
    /**
     * Transform the resource into an array.
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request)
    {
        return ['data' => $this->collection,];
    }

    /**
     * @param Request $request
     * @return array|string[]
     */
    public function with($request)
    {
        return [
            'message' => 'Success',
        ];
    }
}
