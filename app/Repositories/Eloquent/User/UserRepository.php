<?php

namespace App\Repositories\Eloquent\User;

use App\Models\User;
use App\Repositories\Eloquent\BaseRepository;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class BaseRepository
 *
 * @package App\Repositories
 */
class UserRepository extends BaseRepository implements UserRepositoryInterface
{
    public function __construct(User $user)
    {
        parent::__construct($user);
    }

    /**
     * @param array $params
     * @return Collection|Model[]|mixed
     */
    public function get($params = [])
    {
        $search = !empty($params['search']) ? $params['search'] : '';

        $query = $this->model;
        if ($search !== '') {
            $query = $query->where('username', 'like', '%' . $search . '%');
        }

        $query = $query->orderBy('id', 'desc');
        $page = !empty($params['page']) ? $params['page'] : 1;
        $limit = !empty($params['limit']) ? $params['limit'] : 25;

        return $query->paginate($limit, ['*'], 'page', $page);
    }
}
