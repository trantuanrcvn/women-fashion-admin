<?php

namespace App\Repositories\Eloquent\User;

use App\Repositories\Eloquent\RepositoryInterface;

/**
 * Interface UserRepositoryInterface
 *
 * @package App\Repositories\User
 */
interface UserRepositoryInterface extends RepositoryInterface
{
}
