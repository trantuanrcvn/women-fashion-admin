<?php

namespace App\Repositories\Eloquent\Admin;

use App\Repositories\Eloquent\RepositoryInterface;

/**
 * Interface UserRepositoryInterface
 *
 * @package App\Repositories\User
 */
interface AdminRepositoryInterface extends RepositoryInterface
{

}
