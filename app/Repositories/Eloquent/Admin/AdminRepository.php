<?php

namespace App\Repositories\Eloquent\Admin;

use App\Models\Admin;
use App\Repositories\Eloquent\BaseRepository;

/**
 * Class BaseRepository
 *
 * @package App\Repositories
 */
class AdminRepository extends BaseRepository implements AdminRepositoryInterface
{
    public function __construct(Admin $admin)
    {
        parent::__construct($admin);
    }
}
