import React, {Component} from 'react';
import {Redirect, Route} from 'react-router-dom';
import PropTypes from 'prop-types';
import {connect} from "react-redux";
import {authAction} from "../../common/Reducers/AuthReducer";
import {processAction} from "../../common/Reducers/ProcessReducer";

class PrivateRouter extends Component {
    constructor(props) {
        super(props);
        this.props.resetProcessAction();
    }

    componentDidMount() {
        //this.props.refresh();
        /*window.Laravel.timeRefresh = setInterval(() => {
            //If found data user -> call refresh
            if (this.props.dataUser !== null) {
                this.props.refresh();
            }
        }, 5 * 1000);*/
    }

    render() {
        const {authReducer} = this.props;
        const {layout: Layout, component: Component, ...rest} = this.props;
        return (
            authReducer.dataResponse.authentication === null
            || authReducer.dataResponse.authentication === undefined ?
                <Redirect to="/login"/> :
                <Route
                    {...rest}
                    render={matchProps => (
                        <Layout>
                            <Component {...matchProps} />
                        </Layout>
                    )}
                />
        );
    }

}

const mapStateToProps = (state) => {
    return {
        authReducer: state.authReducer
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        refresh: () => {
            dispatch(authAction.refresh())
        },
        resetProcessAction: () => {
            dispatch(processAction.resetProcessAction())
        }
    }
}

PrivateRouter.propTypes = {
    component: PropTypes.any.isRequired,
    layout: PropTypes.any.isRequired,
    path: PropTypes.string
};

export default connect(mapStateToProps, mapDispatchToProps)(PrivateRouter);
