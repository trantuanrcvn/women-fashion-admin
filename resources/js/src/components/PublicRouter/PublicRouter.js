import React, {Component} from 'react';
import {Route, Redirect} from 'react-router-dom';
import PropTypes from 'prop-types';
import {connect} from "react-redux";
import {processAction} from "../../common/Reducers/ProcessReducer";

class PublicRouter extends Component {
    constructor(props) {
        super(props);
        this.props.resetProcessAction();
    }

    render() {
        const {layout: Layout, component: Component, ...rest} = this.props;
        const {authReducer} = this.props;
        return (
            authReducer.dataResponse.authentication !== null
            && authReducer.dataResponse.authentication !== undefined ?
                <Redirect to="/"/> :
                <Route
                    {...rest}
                    render={matchProps => (
                        <Layout>
                            <Component {...matchProps} />
                        </Layout>
                    )}
                />
        );
    }

}

const mapDispatchToProps = (dispatch) => {
    return {
        resetProcessAction: () => {
            dispatch(processAction.resetProcessAction())
        }
    }
}

PublicRouter.propTypes = {
    component: PropTypes.any.isRequired,
    layout: PropTypes.any.isRequired,
    path: PropTypes.string
};

const mapStateToProps = (state) => {
    return {
        authReducer: state.authReducer
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(PublicRouter);
