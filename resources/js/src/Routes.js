import React, {Component} from "react";
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Redirect,
    Link
} from "react-router-dom";
import PrivateRouter from "./components/PrivateRouter";
import PublicRouter from "./components/PublicRouter";
import {
    Main as MainLayout,
    Minimal as MinimalLayout
} from "./layouts";

import {
    Login as LoginView,
    Register as RegisterView,
    Dashboard as DashboardView,
    Users as UsersView,
} from './views';

export default class Routes extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <Switch>
                <PublicRouter component={LoginView} layout={MinimalLayout} exact path="/login"/>
                <PublicRouter component={RegisterView} layout={MinimalLayout} exact path="/register"/>

                <PrivateRouter component={DashboardView} layout={MainLayout} exact path="/"/>
                <PrivateRouter component={DashboardView} layout={MainLayout} exact path="/dashboard"/>
                <PrivateRouter component={UsersView} layout={MainLayout} exact path="/users"/>
            </Switch>
        )
    }
}
