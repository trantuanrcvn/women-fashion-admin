import React, {Component} from 'react';
import {withStyles} from '@material-ui/styles';

import {connect} from 'react-redux'

const useStyles = theme => ({
    root: {
        padding: theme.spacing(4)
    }
});


class Dashboard extends Component {
    constructor(props) {
        super(props);
        this.state = {};
        this.handleChangeInput = this.handleChangeInput.bind(this);
    }

    handleChangeInput(event) {
        event.preventDefault();
        let param = {};
        param[event.target.name] = event.target.value;

        //this.setState(param);
    }

    render() {
        const {classes} = this.props;
        return (
            <div className={classes.root}>
                <div>
                    Dashboard
                </div>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        dataSource: state.dataSource,
        dataUser: state.dataUser,
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        logOut: () => {

        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(withStyles(useStyles)(Dashboard));
