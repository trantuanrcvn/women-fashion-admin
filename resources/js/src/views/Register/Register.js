import React, {Component} from 'react';
import {Link as RouterLink} from 'react-router-dom';
import PropTypes from 'prop-types';
import {withStyles} from '@material-ui/styles';
import {
    Grid,
    Button,
    TextField,
    Link,
    Checkbox,
    Typography
} from '@material-ui/core';
import {connect} from "react-redux";
import {authAction} from "../../common/Reducers/AuthReducer";
import {errorAction} from "../../common/Reducers/ErrorReducer";

const useStyles = theme => ({
    root: {
        backgroundColor: theme.palette.background.default,
        height: '100%'
    },
    grid: {
        height: '100%'
    },
    quoteContainer: {
        [theme.breakpoints.down('md')]: {
            display: 'none'
        }
    },
    quote: {
        backgroundColor: theme.palette.neutral,
        height: '100%',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundImage: 'url(/images/auth.jpg)',
        backgroundSize: 'cover',
        backgroundRepeat: 'no-repeat',
        backgroundPosition: 'center'
    },
    quoteInner: {
        textAlign: 'center',
        flexBasis: '600px'
    },
    quoteText: {
        color: theme.palette.white,
        fontWeight: 300
    },
    name: {
        marginTop: theme.spacing(3),
        color: theme.palette.white
    },
    bio: {
        color: theme.palette.white
    },
    contentContainer: {},
    content: {
        height: '100%',
        display: 'flex',
        flexDirection: 'column'
    },
    contentHeader: {
        display: 'flex',
        alignItems: 'center',
        paddingTop: theme.spacing(5),
        paddingBottom: theme.spacing(2),
        paddingLeft: theme.spacing(2),
        paddingRight: theme.spacing(2)
    },
    logoImage: {
        marginLeft: theme.spacing(4)
    },
    contentBody: {
        flexGrow: 1,
        display: 'flex',
        alignItems: 'center',
        [theme.breakpoints.down('md')]: {
            justifyContent: 'center'
        }
    },
    form: {
        paddingLeft: 100,
        paddingRight: 100,
        paddingBottom: 125,
        flexBasis: 700,
        [theme.breakpoints.down('sm')]: {
            paddingLeft: theme.spacing(2),
            paddingRight: theme.spacing(2)
        }
    },
    title: {
        marginTop: theme.spacing(3)
    },
    textField: {
        marginTop: theme.spacing(2)
    },
    policy: {
        marginTop: theme.spacing(1),
        display: 'flex',
        alignItems: 'center'
    },
    policyCheckbox: {
        marginLeft: '-14px'
    },
    signUpButton: {
        margin: theme.spacing(2, 0)
    }
});

class Register extends Component {
    constructor(props) {
        super(props);

        this.state = {
            username: '',
            first_name: '',
            last_name: '',
            email: '',
            password: '',
            policy: false,
        };

        this.handleRegister = this.handleRegister.bind(this);
        this.handleCheck = this.handleCheck.bind(this);
        this.handleChangeInput = this.handleChangeInput.bind(this);
    }

    handleChangeInput(event) {
        event.preventDefault();
        let param = {};
        param[event.target.name] = event.target.value;

        this.setState(param);
    }

    handleCheck(event) {
        let param = {};
        param[event.target.name] = this.state.policy !== true;
        this.setState(param);
    }

    hasError(errors, field) {
        return !!errors[field];
    }

    handleRegister(event) {
        event.preventDefault();
        this.props.register(this.state, 'handleRegister');
    };

    componentDidMount() {
        this.props.resetErrorAction();
    }

    render() {
        let {classes} = this.props;
        let {errorReducer} = this.props;
        let {processReducer} = this.props;
        let errors = errorReducer.dataResponse.errors ? errorReducer.dataResponse.errors : {};
        return (
            <div className={classes.root}>
                <Grid
                    className={classes.grid}
                    container>
                    <Grid
                        className={classes.quoteContainer}
                        item
                        lg={5}>
                        <div className={classes.quote}>
                            <div className={classes.quoteInner}>
                                <Typography
                                    className={classes.quoteText}
                                    variant="h1">
                                    Hella narwhal Cosby sweater McSweeney's, salvia kitsch before
                                    they sold out High Life.
                                </Typography>
                                <div className={classes.person}>
                                    <Typography
                                        className={classes.name}
                                        variant="body1">
                                        Takamaru Ayako
                                    </Typography>
                                    <Typography
                                        className={classes.bio}
                                        variant="body2">
                                        Manager at inVision
                                    </Typography>
                                </div>
                            </div>
                        </div>
                    </Grid>
                    <Grid
                        className={classes.content}
                        item
                        lg={7}
                        xs={12}>
                        <div className={classes.content}>
                            <div className={classes.contentBody}>
                                <form
                                    className={classes.form}
                                    onSubmit={this.handleRegister}>
                                    <Typography
                                        className={classes.title}
                                        variant="h2">
                                        Create new account
                                    </Typography>
                                    <Typography
                                        color="textSecondary"
                                        gutterBottom>
                                        Use your email to create new account
                                    </Typography>
                                    <TextField
                                        className={classes.textField}
                                        onChange={this.handleChangeInput}
                                        error={this.hasError(errors, 'username')}
                                        fullWidth
                                        helperText={
                                            this.hasError(errors, 'username') ? errors.username[0] : null
                                        }
                                        label="Username"
                                        name="username"
                                        type="text"
                                        value={this.state.username}
                                    />
                                    <TextField
                                        className={classes.textField}
                                        onChange={this.handleChangeInput}
                                        error={this.hasError(errors, 'email')}
                                        fullWidth
                                        helperText={
                                            this.hasError(errors, 'email') ? errors.email[0] : null
                                        }
                                        label="Email address"
                                        name="email"
                                        type="text"
                                        value={this.state.email}
                                    />
                                    <TextField
                                        className={classes.textField}
                                        onChange={this.handleChangeInput}
                                        error={this.hasError(errors, 'first_name')}
                                        fullWidth
                                        helperText={
                                            this.hasError(errors, 'first_name') ? errors.first_name[0] : null
                                        }
                                        label="First name"
                                        name="first_name"
                                        type="text"
                                        value={this.state.first_name}
                                    />
                                    <TextField
                                        className={classes.textField}
                                        onChange={this.handleChangeInput}
                                        error={this.hasError(errors, 'last_name')}
                                        fullWidth
                                        helperText={
                                            this.hasError(errors, 'last_name') ? errors.last_name[0] : null
                                        }
                                        label="Last name"
                                        name="last_name"
                                        type="text"
                                        value={this.state.last_name}
                                    />
                                    <TextField
                                        className={classes.textField}
                                        onChange={this.handleChangeInput}
                                        error={this.hasError(errors, 'password')}
                                        fullWidth
                                        helperText={
                                            this.hasError(errors, 'password') ? errors.password[0] : null
                                        }
                                        label="Password"
                                        name="password"
                                        type="password"
                                        value={this.state.password}
                                    />
                                    <div className={classes.policy}>
                                        <Checkbox
                                            onChange={this.handleCheck}
                                            checked={this.state.policy}
                                            className={classes.policyCheckbox}
                                            color="primary"
                                            name="policy"
                                        />
                                        <Typography
                                            className={classes.policyText}
                                            color="textSecondary"
                                            variant="body1">
                                            I have read the{' '}
                                            <Link
                                                color="primary"
                                                component={RouterLink}
                                                to="#"
                                                underline="always"
                                                variant="h6">
                                                Terms and Conditions
                                            </Link>
                                        </Typography>
                                    </div>
                                    <Button
                                        className={classes.signUpButton}
                                        color="primary"
                                        disabled={processReducer.arrayProcess.indexOf('handleRegister') !== -1 || this.state.policy === false}
                                        fullWidth
                                        size="large"
                                        type="submit"
                                        variant="contained">
                                        Sign up now
                                    </Button>
                                    <Typography
                                        color="textSecondary"
                                        variant="body1">
                                        Have an account?{' '}
                                        <Link
                                            component={RouterLink}
                                            to="/login"
                                            variant="h6">
                                            Sign in
                                        </Link>
                                    </Typography>
                                </form>
                            </div>
                        </div>
                    </Grid>
                </Grid>
            </div>
        )
    }
}

Register.propTypes = {
    history: PropTypes.object
};

const mapStateToProps = (state) => {
    return {
        errorReducer: state.errorReducer,
        processReducer: state.processReducer,
        authReducer: state.authReducer,
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        register: (params, process) => {
            dispatch(authAction.register(params, process))
        },
        resetErrorAction: () => {
            dispatch(errorAction.resetErrorAction())
        }
    }
}


export default connect(mapStateToProps, mapDispatchToProps)(withStyles(useStyles)(Register));
