import React, {Component} from 'react';
import {Link as RouterLink} from 'react-router-dom';
import PropTypes from 'prop-types';
import {withStyles} from '@material-ui/styles';
import {Button, Grid, Link, TextField, Typography} from '@material-ui/core';
import {Facebook as FacebookIcon, Google as GoogleIcon} from '../../icons';

import {connect} from 'react-redux'
import {authAction} from '../../common/Reducers/AuthReducer';
import {errorAction} from '../../common/Reducers/ErrorReducer';

const useStyles = theme => (
    {
        root: {
            backgroundColor: theme.palette.background.default,
            height: '100%'
        },
        grid: {
            height: '100%'
        },
        quoteContainer: {
            [theme.breakpoints.down('md')]: {
                display: 'none'
            }
        },
        quote: {
            backgroundColor: theme.palette.neutral,
            height: '100%',
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center',
            backgroundImage: 'url(/images/auth.jpg)',
            backgroundSize: 'cover',
            backgroundRepeat: 'no-repeat',
            backgroundPosition: 'center'
        },
        quoteInner: {
            textAlign: 'center',
            flexBasis: '600px'
        },
        quoteText: {
            color: theme.palette.white,
            fontWeight: 300
        },
        name: {
            marginTop: theme.spacing(3),
            color: theme.palette.white
        },
        bio: {
            color: theme.palette.white
        },
        contentContainer: {},
        content: {
            height: '100%',
            display: 'flex',
            flexDirection: 'column'
        },
        contentHeader: {
            display: 'flex',
            alignItems: 'center',
            paddingTop: theme.spacing(5),
            paddingBototm: theme.spacing(2),
            paddingLeft: theme.spacing(2),
            paddingRight: theme.spacing(2)
        },
        logoImage: {
            marginLeft: theme.spacing(4)
        },
        contentBody: {
            flexGrow: 1,
            display: 'flex',
            alignItems: 'center',
            [theme.breakpoints.down('md')]: {
                justifyContent: 'center'
            }
        },
        form: {
            paddingLeft: 100,
            paddingRight: 100,
            paddingBottom: 125,
            flexBasis: 700,
            [theme.breakpoints.down('sm')]: {
                paddingLeft: theme.spacing(2),
                paddingRight: theme.spacing(2)
            }
        },
        title: {
            marginTop: theme.spacing(3)
        },
        socialButtons: {
            marginTop: theme.spacing(3)
        },
        socialIcon: {
            marginRight: theme.spacing(1)
        },
        sugestion: {
            marginTop: theme.spacing(2)
        },
        textField: {
            marginTop: theme.spacing(2)
        },
        signInButton: {
            margin: theme.spacing(2, 0)
        }
    })

class Login extends Component {
    constructor(props) {
        super(props);
        this.state = {
            username: 'root',
            password: '1',
        };
        this.handleLogin = this.handleLogin.bind(this);
        this.handleChangeInput = this.handleChangeInput.bind(this);
    }

    handleChangeInput(event) {
        event.preventDefault();
        let param = {};
        param[event.target.name] = event.target.value;

        this.setState(param);
    }

    hasError(errors, field) {
        return !!errors[field];
    }

    handleLogin(event) {
        event.preventDefault();
        this.props.logIn(this.state, 'handleLogin');
    };

    componentDidMount() {
        this.props.resetErrorAction();
    }

    render() {
        let {classes} = this.props;
        let {errorReducer} = this.props;
        let {processReducer} = this.props;
        let errors = errorReducer.dataResponse.errors ? errorReducer.dataResponse.errors : {};
        return (
            <div className={classes.root}>
                <Grid
                    className={classes.grid}
                    container>
                    <Grid
                        className={classes.quoteContainer}
                        item
                        lg={5}>
                        <div className={classes.quote}>
                            <div className={classes.quoteInner}>
                                <Typography
                                    className={classes.quoteText}
                                    variant="h1">
                                    Hella narwhal Cosby sweater McSweeney's, salvia kitsch before
                                    they sold out High Life.
                                </Typography>
                                <div className={classes.person}>
                                    <Typography
                                        className={classes.name}
                                        variant="body1">
                                        Takamaru Ayako
                                    </Typography>
                                    <Typography
                                        className={classes.bio}
                                        variant="body2">
                                        Manager at inVision
                                    </Typography>
                                </div>
                            </div>
                        </div>
                    </Grid>
                    <Grid
                        className={classes.content}
                        item
                        lg={7}
                        xs={12}>
                        <div className={classes.content}>
                            <div className={classes.contentBody}>
                                <form
                                    className={classes.form}>
                                    <Typography
                                        className={classes.title}
                                        variant="h2">
                                        Sign in
                                    </Typography>
                                    <Typography
                                        color="textSecondary"
                                        gutterBottom>
                                        Sign in with social media
                                    </Typography>
                                    <Grid
                                        className={classes.socialButtons}
                                        container
                                        spacing={2}>
                                        <Grid item>
                                            <Button
                                                color="primary"
                                                size="large"
                                                variant="contained">
                                                <FacebookIcon className={classes.socialIcon}/>
                                                Login with Facebook
                                            </Button>
                                        </Grid>
                                        <Grid item>
                                            <Button
                                                size="large"
                                                variant="contained">
                                                <GoogleIcon className={classes.socialIcon}/>
                                                Login with Google
                                            </Button>
                                        </Grid>
                                    </Grid>
                                    <Typography
                                        align="center"
                                        className={classes.sugestion}
                                        color="textSecondary"
                                        variant="body1">
                                        or login with email address
                                    </Typography>
                                    <TextField
                                        className={classes.textField}
                                        fullWidth
                                        type="text"
                                        label="Username"
                                        name="username"
                                        onChange={this.handleChangeInput}
                                        error={this.hasError(errors, 'username')}
                                        helperText={
                                            this.hasError(errors, 'username') ? errors.username[0] : null
                                        }
                                        value={this.state.username}
                                    />
                                    <TextField
                                        className={classes.textField}
                                        fullWidth
                                        label="Password"
                                        name="password"
                                        type="password"
                                        onChange={this.handleChangeInput}
                                        error={this.hasError(errors, 'password')}
                                        helperText={
                                            this.hasError(errors, 'password') ? errors.password[0] : null
                                        }
                                        value={this.state.password}
                                    />
                                    <Button
                                        className={classes.signInButton}
                                        color="primary"
                                        onClick={this.handleLogin}
                                        disabled={processReducer.arrayProcess.indexOf('handleLogin') !== -1}
                                        fullWidth
                                        size="large"
                                        type="submit"
                                        variant="contained">
                                        Sign in now
                                    </Button>
                                    <Typography
                                        color="textSecondary"
                                        variant="body1">
                                        Don't have an account?{' '}
                                        <Link
                                            component={RouterLink}
                                            to="/register"
                                            variant="h6">
                                            Sign up
                                        </Link>
                                    </Typography>
                                </form>
                            </div>
                        </div>
                    </Grid>
                </Grid>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        errorReducer: state.errorReducer,
        processReducer: state.processReducer,
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        logIn: (params, process) => {
            dispatch(authAction.logIn(params, process))
        },
        resetErrorAction: () => {
            dispatch(errorAction.resetErrorAction())
        }
    }
}

Login.propTypes = {
    history: PropTypes.object
};

export default connect(mapStateToProps, mapDispatchToProps)(withStyles(useStyles)(Login));
