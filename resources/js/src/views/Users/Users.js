import React, {Component} from 'react';
import {withStyles} from '@material-ui/styles';
import UsersToolbar from "./components/UsersToolbar";
import UsersTable from "./components/UsersTable";
import UsersDialog from "./components/UsersDialog";
import UsersAlertRemove from "./components/UsersAlertRemove";

const useStyles = theme => ({
    root: {
        padding: theme.spacing(3)
    },
    content: {
        marginTop: theme.spacing(2)
    }
});

class Users extends Component {

    constructor(props) {
        super(props);
        this.state = {
            queryUsers: {
                search: '',
                page: 1,
                limit: 20,
            },
            isOpenDialog: false,
            isOpenAlert: false,
            isUpdated: false,
            currentUserId: null,
            userSelected: []
        };
        this.handleChangeInput = this.handleChangeInput.bind(this);
        this.handleChangePage = this.handleChangePage.bind(this);
        this.handleCloseDialog = this.handleCloseDialog.bind(this);
        this.handleOpenDialog = this.handleOpenDialog.bind(this);
        this.handleCreateUser = this.handleCreateUser.bind(this);

        this.handleEditUser = this.handleEditUser.bind(this);
        this.handleRemoveUser = this.handleRemoveUser.bind(this);
        this.handleSelectUser = this.handleSelectUser.bind(this);
        this.handleSelectAllUser = this.handleSelectAllUser.bind(this);
        this.shouldRefreshTable = this.shouldRefreshTable.bind(this);
        this.handleOpenAlert = this.handleOpenAlert.bind(this);
        this.handleCloseAlert = this.handleCloseAlert.bind(this);
        this.handleSubmitDialog = this.handleSubmitDialog.bind(this);
        this.handleSubmitAlert = this.handleSubmitAlert.bind(this);
    }

    handleChangeInput(e) {
        let queryUsers = this.state.queryUsers;
        this.setState({
            queryUsers: {
                ...queryUsers,
                search: e.target.value
            }
        });
    }

    handleSubmitSearch() {

    }

    shouldRefreshTable() {
        return this.state.isOpenDialog === false && this.state.isOpenAlert === false || this.state.isUpdated === true;
    }

    handleChangePage(event, page) {
        let queryUsers = this.state.queryUsers;
        if (page === queryUsers.page) {
            return;
        }
        this.setState({
            queryUsers: {
                ...queryUsers,
                page: page,
            },
            isUpdated: true
        });
    }

    handleEditUser(e) {
        let state = this.state;
        let userId = e.currentTarget.value;
        this.setState({
            ...state,
            currentUserId: userId,
        }, this.handleOpenDialog);
    }

    handleCreateUser() {
        let state = this.state;
        this.setState({
            ...state,
            currentUserId: null,
        }, this.handleOpenDialog);
    }

    handleRemoveUser(e) {
        let state = this.state;
        let userId = e.currentTarget.value;
        this.setState({
            ...state,
            currentUserId: userId,
        }, this.handleOpenAlert);
    }

    handleOpenDialog() {
        this.setState({
            isOpenDialog: true,
            isUpdated: false,
        });
    }

    handleCloseDialog() {
        let state = this.state;
        this.setState({
            ...state,
            currentUserId: null,
            isOpenDialog: false
        });
    }

    handleSubmitDialog() {
        let state = this.state;
        this.setState({
            ...state,
            currentUserId: null,
            isOpenDialog: false,
            isUpdated: true,
        });
    }

    handleOpenAlert() {
        this.setState({
            isOpenAlert: true,
            isUpdated: false,
        });
    }

    handleCloseAlert() {
        let state = this.state;
        this.setState({
            ...state,
            currentUserId: null,
            isOpenAlert: false
        });
    }

    handleSubmitAlert() {
        let state = this.state;
        this.setState({
            ...state,
            currentUserId: null,
            isOpenAlert: false,
            isUpdated: true,
        });
    }

    handleSelectUser(e) {
        let userSelected = this.state.userSelected;
        let userId = parseInt(e.target.value);
        if (userSelected.indexOf(userId) !== -1) {
            userSelected.splice(userSelected.indexOf(userId), 1);
            this.setState({
                userSelected: [
                    ...userSelected
                ],
            });
        } else {
            this.setState({
                userSelected: [
                    ...userSelected,
                    userId
                ],
            });
        }
    }

    handleSelectAllUser(dataUser) {
        let currentUserSelected = this.state.userSelected;
        let userSelected = [];
        if (dataUser.total === 0) {
            return;
        }
        if (currentUserSelected.length < dataUser.data.length) {
            dataUser.data.map((item, index) => {
                userSelected.push(item.id);
            })

            this.setState({
                userSelected: [
                    ...currentUserSelected,
                    ...userSelected
                ]
            });
        } else {
            userSelected = [];
            this.setState({
                userSelected: []
            });
        }
    }

    render() {
        let {classes} = this.props;
        let {queryUsers, userSelected, currentUserId, isOpenDialog, isOpenAlert} = this.state;
        return (
            <div className={classes.root}>
                <UsersToolbar
                    {...queryUsers}
                    handleChangeInput={this.handleChangeInput}
                    handleSubmitSearch={this.handleSubmitSearch}
                    handleCreateUser={this.handleCreateUser}
                />
                <div className={classes.content}>
                    <UsersTable
                        handleEditUser={this.handleEditUser}
                        handleRemoveUser={this.handleRemoveUser}
                        handleChangePage={this.handleChangePage}
                        handleSelectUser={this.handleSelectUser}
                        handleSelectAllUser={this.handleSelectAllUser}
                        queryUsers={queryUsers}
                        userSelected={userSelected}
                        shouldRefreshTable={this.shouldRefreshTable()}
                    />
                </div>

                {
                    isOpenDialog === false ? null :
                        <UsersDialog
                            currentUserId={currentUserId}
                            handleSubmitDialog={this.handleSubmitDialog}
                            handleCloseDialog={this.handleCloseDialog}
                        />
                }

                {
                    isOpenAlert === false ? null :
                        <UsersAlertRemove
                            currentUserId={currentUserId}
                            handleCloseAlert={this.handleCloseAlert}
                            handleSubmitAlert={this.handleSubmitAlert}
                        />
                }

            </div>
        )
    }
}

export default withStyles(useStyles)(Users);
