import React, {Component} from 'react';
import clsx from 'clsx';
import {withStyles} from '@material-ui/styles';
import {Button, Grid, Input, Paper} from '@material-ui/core';

import {SearchInput} from '../../../../components/SearchInput';
import SearchIcon from "@material-ui/icons/Search";

const useStyles = theme => ({
    root: {},
    row: {
        height: '42px',
        display: 'flex',
        alignItems: 'center',
        marginTop: theme.spacing(1)
    },
    spacer: {
        flexGrow: 1
    },
    importButton: {
        marginRight: theme.spacing(1)
    },
    exportButton: {
        marginRight: theme.spacing(1)
    },
    searchInput: {
        borderRadius: '4px 0 0 4px'
    },
    submitSearch: {
        padding: theme.spacing(0.9),
        borderRadius: '0 4px 4px 0'
    },
})

class UsersToolbar extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        let {classes, className, handleCreateUser} = this.props;
        return (
            <div
                className={clsx(classes.root, className)}>
                <div className={classes.row}>
                    <span className={classes.spacer}/>
                    <Button className={classes.importButton}>Import</Button>
                    <Button className={classes.exportButton}>Export</Button>
                    <Button
                        onClick={handleCreateUser}
                        color="primary"
                        variant="contained">
                        Add user
                    </Button>
                </div>
                <div className={classes.row}>
                    <SearchInput
                        className={classes.searchInput}
                        placeholder="Search user"
                        onChange={this.props.handleChangeInput}
                    />
                    <Button
                        className={classes.submitSearch}
                        color="primary"
                        variant="contained"
                        onClick={this.props.handleSubmitSearch}>
                        <SearchIcon className={classes.icon}/>
                    </Button>
                </div>
            </div>
        );
    }
}

export default withStyles(useStyles)(UsersToolbar);
