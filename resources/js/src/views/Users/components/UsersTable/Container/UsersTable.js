import React, {Component} from 'react';
import {connect} from "react-redux";

import {apiAction} from "../../../../../common/Reducers/ApiReducer";
import {default as UsersTablePresentational} from "../Presentational/UsersTable"

class UsersTable extends Component {

    constructor(props) {
        super(props);
        this.state = {};
        this.handleGetUser = this.handleGetUser.bind(this);
    }

    handleGetUser(params) {
        this.props.getData(window.Laravel.apiUrl + '/users', params);
    }

    componentDidMount() {
        this.handleGetUser(this.props.queryUsers);
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        let {apiReducer} = this.props;

        if (apiReducer.dataResponse !== undefined) {
            if (_.isEmpty(apiReducer.dataResponse.meta)) {
                this.handleGetUser(this.props.queryUsers);
            }
        }
        if (this.props.queryUsers !== prevProps.queryUsers) {
            this.handleGetUser(this.props.queryUsers)
        }
    }

    shouldComponentUpdate(nextProps, nextState, nextContext) {
        let {shouldRefreshTable} = nextProps;
        return shouldRefreshTable;
    }

    render() {
        let {apiReducer} = this.props;
        let users = [];
        let meta = [];
        if (apiReducer.dataResponse !== undefined) {
            if (apiReducer.dataResponse.data !== undefined && apiReducer.dataResponse.meta !== undefined) {
                users = apiReducer.dataResponse.data
                meta = apiReducer.dataResponse.meta
            }
        }
        return (
            <UsersTablePresentational
                {...this.props}
                users={users}
                meta={meta}
            />
        )
    }
}

const mapStateToProps = (state) => {
    return {
        apiReducer: state.apiReducer,
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        getData: (url, params, processes) => {
            dispatch(apiAction.getData(url, params, processes));
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(UsersTable);
