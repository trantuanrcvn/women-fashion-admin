import React, {Component} from 'react';
import {Pagination} from '@material-ui/lab';

import clsx from 'clsx';
import PerfectScrollbar from 'react-perfect-scrollbar';
import {withStyles} from '@material-ui/styles';
import {
    Card,
    CardActions,
    CardContent,
    Avatar,
    Checkbox,
    Table,
    TableBody,
    TableCell,
    TableHead,
    TableRow,
} from '@material-ui/core';
import IconButton from "@material-ui/core/IconButton";
import DeleteIcon from '@material-ui/icons/Delete';
import EditIcon from '@material-ui/icons/Edit';
import _ from "lodash";

const useStyles = theme => ({
    root: {},
    content: {
        padding: 0
    },
    inner: {
        minWidth: 1050
    },
    nameContainer: {
        display: 'flex',
        alignItems: 'center'
    },
    avatar: {
        marginRight: theme.spacing(2)
    },
    actions: {
        justifyContent: 'center'
    }
});

class UsersTable extends Component {

    constructor(props) {
        super(props);
        this.state = {};
    }

    render() {
        let {handleChangePage, userSelected, handleEditUser, handleRemoveUser, handleSelectUser, handleSelectAllUser} = this.props;
        let {classes, className} = this.props;
        let {users, meta} = this.props;

        return (
            <Card
                className={clsx(classes.root, className)}>
                <CardContent className={classes.content}>
                    <PerfectScrollbar>
                        <div className={classes.inner}>
                            <Table>
                                <TableHead>
                                    <TableRow>
                                        <TableCell padding="checkbox">
                                            <Checkbox
                                                checked={userSelected.length === users.length}
                                                color="primary"
                                                indeterminate={
                                                    userSelected.length > 0 &&
                                                    userSelected.length < users.length
                                                }
                                                onChange={((event) => {
                                                    handleSelectAllUser(users);
                                                })}
                                            />
                                        </TableCell>
                                        <TableCell>Action</TableCell>
                                        <TableCell>User name</TableCell>
                                        <TableCell>Email</TableCell>
                                        <TableCell>Full name</TableCell>
                                        <TableCell>Registration date</TableCell>
                                    </TableRow>
                                </TableHead>
                                <TableBody>
                                    {users.map((user, index) => (
                                        <TableRow key={index}>
                                            <TableCell padding="checkbox">
                                                <Checkbox
                                                    value={user.id}
                                                    onChange={handleSelectUser}
                                                    color="primary"
                                                    checked={userSelected.indexOf(user.id) !== -1}
                                                />
                                            </TableCell>
                                            <TableCell>
                                                <IconButton
                                                    aria-label="Edit"
                                                    value={user.id}
                                                    onClick={handleEditUser}
                                                    className={classes.margin}>
                                                    <EditIcon/>
                                                </IconButton>
                                                <IconButton
                                                    value={user.id}
                                                    onClick={handleRemoveUser}
                                                    className={classes.margin}
                                                    aria-label="delete">
                                                    <DeleteIcon/>
                                                </IconButton>
                                            </TableCell>
                                            <TableCell>{user.username}</TableCell>
                                            <TableCell>{user.email}</TableCell>
                                            <TableCell>{user.first_name + ' ' + user.last_name}</TableCell>
                                            <TableCell>{user.created_at}</TableCell>
                                        </TableRow>
                                    ))}
                                </TableBody>
                            </Table>
                        </div>
                    </PerfectScrollbar>
                </CardContent>
                <CardActions className={classes.actions}>
                    <Pagination
                        count={meta.last_page}
                        color="primary"
                        shape="rounded"
                        showFirstButton
                        showLastButton
                        onChange={handleChangePage}
                    />
                </CardActions>
            </Card>
        )
    }
}

export default withStyles(useStyles)(UsersTable);
