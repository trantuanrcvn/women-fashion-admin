import React, {Component} from 'react';
import {connect} from "react-redux";
import {apiAction} from "../../../../../common/Reducers/ApiReducer";
import {default as UsersAlertRemovePresentational} from "../Presentational/UsersAlertRemove";


class UsersAlertRemove extends Component {

    constructor(props) {
        super(props);
        this.state = {
            id: null,
            username: '',
            password: '',
            first_name: '',
            last_name: '',
            email: '',
        }
        this.handleGetUser = this.handleGetUser.bind(this);
        this.handleDeleteUser = this.handleDeleteUser.bind(this);
    }

    handleDeleteUser(e) {
        e.preventDefault();
        let url = window.Laravel.apiUrl + '/users/' + e.currentTarget.value;
        this.props.deleteData(url);
        this.props.handleSubmitAlert();
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        let {apiReducer} = this.props;
        if (apiReducer.dataResponse.data === undefined) {
            return;
        }
        if (apiReducer.dataResponse.data !== prevProps.apiReducer.dataResponse.data) {
            let currentState = this.state;
            if (apiReducer.dataResponse.data.id !== undefined) {
                this.setState({
                    ...currentState,
                    ...apiReducer.dataResponse.data
                });
            }
        }
    }

    handleGetUser(userId) {
        let url = window.Laravel.apiUrl + '/users/' + userId;
        this.props.getData(url, {})
    }

    componentDidMount() {
        this.handleGetUser(this.props.currentUserId);
    }

    render() {
        let currentUser = {...this.state};

        if (currentUser.id === null) {
            return null;
        }
        return (
            <UsersAlertRemovePresentational
                handleDeleteUser={this.handleDeleteUser}
                currentUser={currentUser}
                {...this.props}
            />
        )
    }
}

const mapStateToProps = (state) => {
    return {
        apiReducer: state.apiReducer
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        deleteData: (url, processes) => {
            dispatch(apiAction.deleteData(url, processes));
        },
        getData: (url, params, processes) => {
            dispatch(apiAction.getData(url, params, processes));
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(UsersAlertRemove);
