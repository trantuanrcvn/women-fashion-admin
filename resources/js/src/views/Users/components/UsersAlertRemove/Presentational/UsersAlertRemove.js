import React, {Component} from 'react';
import {withStyles} from '@material-ui/styles';
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogActions from "@material-ui/core/DialogActions";

const useStyles = theme => ({
    root: {},
});

class UsersAlertRemove extends Component {

    constructor(props) {
        super(props);
        this.state = {}
    }


    render() {
        let {handleCloseAlert, handleDeleteUser} = this.props;
        let {currentUser} = this.props;
        return (
            <Dialog
                open={true}
                onClose={handleCloseAlert}
                aria-labelledby="alert-dialog-title"
                aria-describedby="alert-dialog-description">
                <DialogTitle id="alert-dialog-title">{"Use Google's location service?"}</DialogTitle>
                <DialogContent>
                    <DialogContentText id="alert-dialog-description">
                        Do you want remove this user? {currentUser.username}
                    </DialogContentText>
                </DialogContent>
                <DialogActions>
                    <Button value={currentUser.id} onClick={handleDeleteUser} color="primary" autoFocus>
                        Agree
                    </Button>
                    <Button onClick={handleCloseAlert} color="primary">
                        Disagree
                    </Button>
                </DialogActions>
            </Dialog>
        )
    }
}

export default withStyles(useStyles)(UsersAlertRemove);

