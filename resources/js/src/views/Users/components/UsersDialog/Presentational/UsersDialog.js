import React, {Component} from 'react';
import {withStyles} from '@material-ui/styles';
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogActions from "@material-ui/core/DialogActions";
import TextField from "@material-ui/core/TextField";

const useStyles = theme => ({
    root: {},
});

class UsersDialog extends Component {

    constructor(props) {
        super(props);
        this.state = {}
    }


    render() {
        let {handleCloseDialog, handleChangeInput, handleSubmitUser} = this.props;
        let {currentUser} = this.props;
        return (
            <div>
                <Dialog open={true} onClose={handleCloseDialog} aria-labelledby="form-dialog-title">
                    <form onSubmit={handleSubmitUser}>
                        <DialogTitle>Form user</DialogTitle>
                        <DialogContent>
                            <DialogContentText>
                                Enter information to create
                            </DialogContentText>
                            <TextField
                                autoFocus
                                margin="dense"
                                id="username"
                                name="username"
                                value={currentUser.username}
                                onChange={handleChangeInput}
                                label="Username"
                                type="text"
                                fullWidth
                            />
                            <TextField
                                autoFocus
                                margin="dense"
                                id="password"
                                name="password"
                                onChange={handleChangeInput}
                                label="Password"
                                type="password"
                                fullWidth
                            />
                            <TextField
                                autoFocus
                                margin="dense"
                                name="first_name"
                                id="first_name"
                                value={currentUser.first_name}
                                onChange={handleChangeInput}
                                label="First name"
                                type="text"
                                fullWidth
                            />
                            <TextField
                                autoFocus
                                margin="dense"
                                id="last_name"
                                name="last_name"
                                value={currentUser.last_name}
                                onChange={handleChangeInput}
                                label="Last name"
                                type="text"
                                fullWidth
                            />
                            <TextField
                                autoFocus
                                margin="dense"
                                id="email"
                                name="email"
                                value={currentUser.email}
                                onChange={handleChangeInput}
                                label="Email Address"
                                type="email"
                                fullWidth
                            />
                        </DialogContent>
                        <DialogActions>
                            <Button
                                type="submit"
                                color="primary">
                                {currentUser.id === null ?
                                    'Create' : 'Update'
                                }
                            </Button>
                            <Button onClick={handleCloseDialog} color="primary">
                                Cancel
                            </Button>
                        </DialogActions>
                    </form>
                </Dialog>
            </div>
        )
    }
}

export default withStyles(useStyles)(UsersDialog);

