import React, {Component} from 'react';
import {connect} from "react-redux";
import {apiAction} from "../../../../../common/Reducers/ApiReducer";
import {default as UsersDialogPresentational} from "../Presentational/UsersDialog";


class UsersDialog extends Component {

    constructor(props) {
        super(props);
        this.state = {
            id: null,
            username: '',
            password: '',
            first_name: '',
            last_name: '',
            email: '',
        }
        this.handleGetUser = this.handleGetUser.bind(this);
        this.handleChangeInput = this.handleChangeInput.bind(this);
        this.handleSubmitUser = this.handleSubmitUser.bind(this);
    }

    handleChangeInput(event) {
        event.preventDefault();
        let param = {};
        param[event.target.name] = event.target.value;
        let currentState = this.state;
        this.setState({
            ...currentState,
            ...param
        });
    }

    handleSubmitUser(e) {
        e.preventDefault();
        let params = this.state;
        let url = window.Laravel.apiUrl + '/users'
        if (this.state.id === null) {
            this.props.postData(url, params);
        } else {
            url += '/' + params.id;
            this.props.putData(url, params);
        }
        this.props.handleSubmitDialog();
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        let {apiReducer} = this.props;
        if (apiReducer.dataResponse.data !== prevProps.apiReducer.dataResponse.data) {
            let currentState = this.state;
            if (apiReducer.dataResponse.data.id !== undefined) {
                this.setState({
                    ...currentState,
                    ...apiReducer.dataResponse.data
                });
            }
        }
    }

    handleGetUser(userId) {
        let url = window.Laravel.apiUrl + '/users/' + userId;
        this.props.getData(url, {})
    }

    componentDidMount() {
        if (this.props.currentUserId !== null) {
            this.handleGetUser(this.props.currentUserId);
        }
    }

    render() {
        let currentUser = {...this.state};

        return (
            <UsersDialogPresentational
                handleChangeInput={this.handleChangeInput}
                handleSubmitUser={this.handleSubmitUser}
                currentUser={currentUser}
                {...this.props}
            />
        )
    }
}

const mapStateToProps = (state) => {
    return {
        apiReducer: state.apiReducer
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        postData: (url, params, processes) => {
            dispatch(apiAction.postData(url, params, processes));
        },
        putData: (url, params, processes) => {
            dispatch(apiAction.putData(url, params, processes));
        },
        getData: (url, params, processes) => {
            dispatch(apiAction.getData(url, params, processes));
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(UsersDialog);
