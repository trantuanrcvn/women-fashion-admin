import React, {Component} from 'react';
import {Link as RouterLink} from 'react-router-dom';
import clsx from 'clsx';
import {withStyles} from '@material-ui/styles';
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";

const useStyles = (theme) => ({
    root: {
        /*boxShadow: 'none'*/
    },
});

class TopBar extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        let {classes} = this.props;
        let {rest} = this.props;
        let {className} = this.props;
        return (
            <AppBar
                {...rest}
                className={clsx(classes.root, className)}
                color="primary"
                position="fixed">
                <Toolbar>
                    <RouterLink to="/">
                        <img
                            alt="Logo"
                            src="/images/logos/logo--white.svg"
                        />
                    </RouterLink>
                </Toolbar>
            </AppBar>
        )
    }
}

export default withStyles(useStyles)(TopBar);
