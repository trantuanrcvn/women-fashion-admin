import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {withStyles} from '@material-ui/styles';
import {TopBar} from './components';
import {connect} from 'react-redux'
import LinearProgress from "@material-ui/core/LinearProgress";
import Snackbar from '@material-ui/core/Snackbar';
import MuiAlert, {AlertProps} from '@material-ui/lab/Alert';

import {notificationAction} from "../../common/Reducers/NotificationReducer";
import AppBar from "@material-ui/core/AppBar";
import clsx from "clsx";

const useStyles = (theme) => ({
    root: {
        paddingTop: 64,
        height: '100%'
    },
    content: {
        height: '100%'
    },
    linearProgress: {
        width: '100%',
        '& > * + *': {
            marginTop: theme.spacing(2),
        },
        position: 'fixed',
        height: '2px',
        top: 0,
        left: 0,
        zIndex: 1201,
        backgroundColor: '#fff'
    },
});

function styleAnchorOrigin() {
    return {
        vertical: 'top',
        horizontal: 'right',
    }
}

function Alert(AlertProps) {
    return <MuiAlert elevation={6} variant="filled" {...AlertProps} />;
}

class Minimal extends Component {
    constructor(props) {
        super(props);
        this.state = {};
        this.handlePopNotification = this.handlePopNotification.bind(this);
    }

    handlePopNotification() {
        this.props.popNotificationAction();
    }

    render() {
        let {classes, children} = this.props;

        let {processReducer, notificationReducer} = this.props;
        let currentNotification = notificationReducer.currentNotification;
        return (
            <div className={classes.root}>
                <TopBar/>
                <main className={classes.content}>
                    {children}
                </main>

                <LinearProgress
                    className={classes.linearProgress}
                    hidden={processReducer.totalProcess <= 0}/>
                {currentNotification.type !== '' ?
                    <Snackbar
                        open={currentNotification.type !== ''}
                        onClose={this.handlePopNotification}
                        autoHideDuration={3000}
                        anchorOrigin={styleAnchorOrigin()}>
                        <Alert
                            onClose={this.handlePopNotification}
                            severity={currentNotification.type}>
                            {currentNotification.message}
                        </Alert>
                    </Snackbar> : null
                }
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        processReducer: state.processReducer,
        notificationReducer: state.notificationReducer,
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        popNotificationAction: () => {
            dispatch(notificationAction.popNotificationAction())
        }
    }
}

Minimal.propTypes = {
    children: PropTypes.node,
    className: PropTypes.string
};

export default connect(mapStateToProps, mapDispatchToProps)(withStyles(useStyles)(Minimal));
