import React, {Component} from 'react';
import {withStyles} from '@material-ui/styles';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import LinearProgress from "@material-ui/core/LinearProgress";
import Snackbar from '@material-ui/core/Snackbar';
import MuiAlert, {AlertProps} from '@material-ui/lab/Alert';
import withWidth, {isWidthUp} from '@material-ui/core/withWidth';

import {Sidebar, TopBar, Footer} from './components';
import {connect} from 'react-redux'
import {notificationAction} from "../../common/Reducers/NotificationReducer";
import {processAction} from "../../common/Reducers/ProcessReducer";

const useStyles = (theme) => ({
    root: {
        paddingTop: 56,
        height: '100%',
        [theme.breakpoints.up('sm')]: {
            paddingTop: 64
        }
    },
    shiftContent: {
        paddingLeft: 240
    },
    content: {
        height: '100%'
    },
    linearProgress: {
        width: '100%',
        '& > * + *': {
            marginTop: theme.spacing(2),
        },
        position: 'fixed',
        height: '2px',
        top: 0,
        left: 0,
        zIndex: 1201,
        backgroundColor: '#fff'
    },
});


function styleAnchorOrigin() {
    return {
        vertical: 'top',
        horizontal: 'right',
    }
}

function Alert(AlertProps) {
    return <MuiAlert elevation={6} variant="filled" {...AlertProps} />;
}

class Main extends Component {
    constructor(props) {
        super(props);
        this.state = {
            openSidebar: false
        };
        this.handlePopNotification = this.handlePopNotification.bind(this);
        this.isDesktop = this.isDesktop.bind(this);
        this.shouldOpenSidebar = this.shouldOpenSidebar.bind(this);
        this.handleSidebarOpen = this.handleSidebarOpen.bind(this);
        this.handleSidebarClose = this.handleSidebarClose.bind(this);
    }

    handlePopNotification() {
        this.props.popNotificationAction();
    }

    isDesktop() {
        return isWidthUp('md', this.props.width);
    }

    shouldOpenSidebar() {
        return this.isDesktop() ? true : this.state.openSidebar;
    }

    handleSidebarOpen() {
        this.setState({
            openSidebar: true
        })
    }

    handleSidebarClose() {
        this.setState({
            openSidebar: false
        })
    }

    render() {
        let {classes, children} = this.props;

        let {processReducer, notificationReducer} = this.props;
        let currentNotification = notificationReducer.currentNotification;

        return (
            <div
                className={clsx({
                    [classes.root]: true,
                    [classes.shiftContent]: this.isDesktop()
                })}>
                <TopBar onSidebarOpen={this.handleSidebarOpen}/>
                <Sidebar
                    onClose={this.handleSidebarClose}
                    open={this.shouldOpenSidebar()}
                    variant={this.isDesktop() ? 'persistent' : 'temporary'}
                />
                <main className={classes.content}>
                    {children}
                    <Footer/>
                </main>

                <LinearProgress
                    className={classes.linearProgress}
                    hidden={processReducer.totalProcess <= 0}/>
                {currentNotification.type !== '' ?
                    <Snackbar
                        open={currentNotification.type !== ''}
                        onClose={this.handlePopNotification}
                        autoHideDuration={3000}
                        anchorOrigin={styleAnchorOrigin()}>
                        <Alert
                            onClose={this.handlePopNotification}
                            severity={currentNotification.type}>
                            {currentNotification.message}
                        </Alert>
                    </Snackbar> : null
                }
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        processReducer: state.processReducer,
        notificationReducer: state.notificationReducer,
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        popNotificationAction: () => {
            dispatch(notificationAction.popNotificationAction())
        },
    }
}

Main.propTypes = {
    children: PropTypes.node
};

export default connect(mapStateToProps, mapDispatchToProps)(withStyles(useStyles)(withWidth()(Main)));
