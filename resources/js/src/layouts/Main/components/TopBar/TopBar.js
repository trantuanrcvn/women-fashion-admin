import React, {Component} from 'react';
import {Link as RouterLink} from 'react-router-dom';
import clsx from 'clsx';
import {AppBar, Toolbar, Badge, Hidden, IconButton} from '@material-ui/core';
import MenuIcon from '@material-ui/icons/Menu';
import NotificationsIcon from '@material-ui/icons/NotificationsOutlined';
import InputIcon from '@material-ui/icons/Input';
import {withStyles} from '@material-ui/styles';

import {connect} from 'react-redux';
import {authAction} from "../../../../common/Reducers/AuthReducer";

const useStyles = theme => ({
    root: {
        background: '#fff',
    },
    flexGrow: {
        flexGrow: 1
    },
    signOutButton: {
        marginLeft: theme.spacing(1)
    },
});

class TopBar extends Component {
    constructor(props) {
        super(props);
        this.state = {};
        this.handleLogOut = this.handleLogOut.bind(this);
    }

    handleLogOut() {
        this.props.logOut();
    };

    render() {
        let {classes} = this.props;
        let {className} = this.props;
        return (
            <AppBar
                className={clsx(classes.root, className)}>
                <Toolbar>
                    <RouterLink to="/">
                        <img alt="Logo" src="/images/logos/logo--blue.svg"/>
                    </RouterLink>
                    <div className={classes.flexGrow}/>
                    <Hidden smDown>
                        <IconButton>
                            <Badge
                                /*badgeContent={notifications.length}*/
                                color="primary" variant="dot">
                                <NotificationsIcon/>
                            </Badge>
                        </IconButton>
                        <IconButton
                            className={classes.signOutButton}
                            onClick={this.handleLogOut}>
                            <InputIcon/>
                        </IconButton>
                    </Hidden>
                    <Hidden mdUp>
                        <IconButton onClick={this.props.onSidebarOpen}><MenuIcon/>
                        </IconButton>
                    </Hidden>
                </Toolbar>
            </AppBar>
        )
    }
}

const mapStateToProps = (state) => {
    return {}
}

const mapDispatchToProps = (dispatch) => {
    return {
        logOut: () => {
            dispatch(authAction.logOut())
        },
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(withStyles(useStyles)(TopBar));

