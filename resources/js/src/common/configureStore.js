import React from "react";
import {createLogger} from 'redux-logger';
import {applyMiddleware, createStore} from 'redux';
import rootReducer from './Reducers';
import thunkMiddleware from 'redux-thunk';

import {notificationAction} from './Reducers/NotificationReducer';
import {processAction} from './Reducers/ProcessReducer';
import {authAction} from './Reducers/AuthReducer';
import {errorAction} from './Reducers/ErrorReducer';

export default function configureStore(preloadedState) {
    return createStore(
        rootReducer,
        preloadedState,
        applyMiddleware(
            thunkMiddleware,
            loggerMiddleware
        )
    )
}

//Config redux-logger
const loggerMiddleware = createLogger({
    predicate: (getState, action) => arrayActionSkipLogger.indexOf(action.type) === -1,
    collapsed: (getState, action) => arrayActionCollapsed.indexOf(action.type) !== -1,
});

const arrayActionSkipLogger = [
    notificationAction.RESET_NOTIFICATION_ACTION,
    notificationAction.SKIP_NOTIFICATION_ACTION,
    notificationAction.PUSH_NOTIFICATION_ACTION,
    notificationAction.POP_NOTIFICATION_ACTION,

    processAction.RESET_PROCESS_ACTION,
    processAction.PUSH_PROCESS_ACTION,
    processAction.POP_PROCESS_ACTION,
]

const arrayActionCollapsed = [
    authAction.LOGIN_ACTION,
    authAction.LOGOUT_ACTION,
    authAction.REFRESH_ACTION,
    authAction.REGISTER_ACTION,

    errorAction.RESET_ERROR_ACTION,
    errorAction.GET_ERROR_ACTION,
]
