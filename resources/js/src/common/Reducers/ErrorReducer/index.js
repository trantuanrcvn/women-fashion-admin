export const errorAction = require('./action');
import {authStore} from '../AuthReducer'

function initState() {
    return {
        'dataResponse': {
            errors: {},
            message: '',
        },
        'status': null,
    }
}

export function errorReducer(state = initState(), action) {
    switch (action.type) {
        case errorAction.GET_ERROR_ACTION:
            switch (action.payload.status) {
                case 401:
                    authStore.removeUser();
                    location.reload();
                    break;
                case 429:
                    authStore.removeUser();
                    location.reload();
                    break;
                default:
                    break;
            }
            return action.payload;
        case errorAction.RESET_ERROR_ACTION:
            return initState();
        default:
            return state;
    }
}

