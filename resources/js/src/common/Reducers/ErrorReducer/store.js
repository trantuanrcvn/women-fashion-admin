const varArrayError = 'array_error';

export function getTotalError() {
    let arrayError = getError();
    return arrayError.length;
}

export function getError() {
    let processesString = sessionStorage.getItem(varArrayError);
    if (processesString === null) {
        resetError();
    }
    try {
        if (processesString === '') {
            return [];
        }
        return processesString.split(',');
    } catch (e) {
        return [];
    }
}

export function resetError() {
    sessionStorage.setItem(varArrayError, '');
}

export function pushError(error) {
    let arrayError = getError();
    arrayError.push(error);
    sessionStorage.setItem(varArrayError, arrayError.toString());
}

export function popError(error) {
    let arrayError = getError();
    for (let i = 0; i < arrayError.length; i++) {
        if (arrayError[i] === error) {
            arrayError.splice(i, 1);
            break;
        }
    }
    sessionStorage.setItem(varArrayError, arrayError.toString());
}
