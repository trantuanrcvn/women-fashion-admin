import {errorAction} from "./index";
import {notificationAction} from "../NotificationReducer";
import {processAction} from "../ProcessReducer";

export const GET_ERROR_ACTION = 'GET_ERROR_ACTION';
export const RESET_ERROR_ACTION = 'RESET_ERROR_ACTION';

/*
 * action creators
 */

export function getErrorAction(status, dataResponse) {
    return {
        type: GET_ERROR_ACTION,
        payload: {
            status, dataResponse
        }
    }
}

export function resetErrorAction() {
    return {type: RESET_ERROR_ACTION}
}


export function catchFunction(dispatch, error) {
    dispatch(errorAction.getErrorAction(error.response.status, error.response.data));
    dispatch(notificationAction.pushNotificationAction(error.response));
    dispatch(processAction.resetProcessAction());
}

export function finallyFunction(dispatch, process) {
    dispatch(processAction.popProcessAction(process))
}

export function beginFunction(dispatch, process) {
    dispatch(processAction.pushProcessAction(process))
}

