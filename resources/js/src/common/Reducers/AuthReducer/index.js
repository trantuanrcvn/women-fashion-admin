import {combineReducers} from 'redux';

export const authAction = require('./action');
export const authStore = require('./store');

function initState() {
    return {
        'dataResponse': {
            authentication: authStore.getUser(),
        },
        'status': null,
    }
}

export function authReducer(state = initState(), action) {
    let payload = action.payload;
    switch (action.type) {
        case authAction.LOGIN_ACTION: {
            if (payload.dataResponse.access_token !== undefined) {
                authStore.setUser(payload.dataResponse.user, payload.dataResponse.access_token);
            }
            return initState();
        }
        case authAction.LOGOUT_ACTION: {
            authStore.removeUser();
            return initState();
        }
        case authAction.REGISTER_ACTION: {
            authStore.removeUser();
            if (payload.status === 201) {
                window.location.replace(window.Laravel.baseUrl);
            }
            return initState();
        }
        case authAction.REFRESH_ACTION: {
            if (payload.access_token !== undefined) {
                authStore.setUser(payload.dataResponse.user, payload.dataResponse.access_token);
            }
            return initState();
        }
        default: {
            return state;
        }
    }
}

/*const authReducer = combineReducers({
    authentication,
})*/

/*export default authReducer*/
