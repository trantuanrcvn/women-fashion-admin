import axios from 'axios';
import * as authStore from './store'
import {errorAction} from '../ErrorReducer'

/*
 * action types
 */

export const LOGIN_ACTION = 'LOGIN_ACTION';
export const LOGOUT_ACTION = 'LOGOUT_ACTION';
export const REGISTER_ACTION = 'REGISTER_ACTION';
export const REFRESH_ACTION = 'REFRESH_ACTION';

/*
 * action creators
 */

export function logInAction(status, dataResponse) {
    return {
        type: LOGIN_ACTION,
        payload: {
            status, dataResponse
        }
    }
}

export function logIn(params, process = 'logIn') {
    //authStore.resetCountError();
    let url = window.Laravel.apiUrl + '/auth/login';
    return dispatch => {
        errorAction.beginFunction(dispatch, process)
        return axios.post(url, params)
            .then(response => {
                // Set data response for dataResponse
                dispatch(logInAction(response.status, response.data));
            }).catch(error => {
                errorAction.catchFunction(dispatch, error)
            }).finally(() => {
                errorAction.finallyFunction(dispatch, process)
            });
    }
}

export function logOutAction() {
    return {type: LOGOUT_ACTION}
}

export function logOut(process = 'logout') {
    let url = window.Laravel.apiUrl + '/auth/logout';
    let config = authStore.getConfigHeader();
    return dispatch => {
        errorAction.beginFunction(dispatch, process)
        return axios.post(url, {}, config)
            .then(response => {
                // Set data response for dataResponse
                dispatch(logOutAction());
            }).catch(error => {
                errorAction.catchFunction(dispatch, error)
            }).finally(() => {
                errorAction.finallyFunction(dispatch, process)
            });
    }
}

export function registerAction(status, dataResponse) {
    return {
        type: REGISTER_ACTION,
        payload: {
            status, dataResponse
        }
    }
}

export function register(params, process = 'register') {
    let url = window.Laravel.apiUrl + '/auth/register';
    return dispatch => {
        errorAction.beginFunction(dispatch, process)
        return axios.post(url, params)
            .then(response => {
                // Set data response for dataResponse
                dispatch(registerAction(response.status, response.data));
            }).catch(error => {
                errorAction.catchFunction(dispatch, error)
            }).finally(() => {
                errorAction.finallyFunction(dispatch, process)
            });
    }
}

export function refreshAction(status, dataResponse) {
    //authStore.resetCountError();
    return {
        type: REFRESH_ACTION,
        payload: {
            status, dataResponse
        }
    }
}

export function refresh(process = 'refresh') {
    let url = window.Laravel.apiUrl + '/auth/refresh';
    let config = authStore.getConfigHeader();
    return dispatch => {
        errorAction.beginFunction(dispatch, process)
        return axios.post(url, {}, config)
            .then(response => {
                // Set data response for dataResponse
                dispatch(refreshAction(response.status, response.data));
            }).catch(error => {
                errorAction.catchFunction(dispatch, error)
            }).finally(() => {
                errorAction.finallyFunction(dispatch, process)
            });
    }
}
