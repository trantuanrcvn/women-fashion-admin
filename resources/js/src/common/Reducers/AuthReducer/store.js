export function getConfigHeader(params = {}) {
    return {
        headers: {Authorization: `Bearer ${getToken()}`},
        params
    };
}

const maxErrorRequest = 3;

export function getToken() {
    try {
        return localStorage.getItem('access_token');
    } catch (e) {
        return null;
    }
}

export function setUser(user, accessToken) {
    try {
        let stringUser = JSON.stringify(user);
        localStorage.setItem('user', stringUser);
        localStorage.setItem('access_token', accessToken);
    } catch (e) {
        localStorage.removeItem('user');
        localStorage.removeItem('access_token');
    }
}

export function getUser() {
    try {
        let stringUser = localStorage.getItem('user');
        return JSON.parse(stringUser);
    } catch (e) {
        return null;
    }
}

export function removeUser() {
    localStorage.removeItem('user');
    localStorage.removeItem('access_token');
}

export function isReloadPage() {
    if (getCountErrorRequest() >= maxErrorRequest) {
        resetCountError();
        return true;
    }
    return false;
}

export function getCountErrorRequest() {
    let totalErrorRequestString = localStorage.getItem('total_error_request');
    if (totalErrorRequestString === null) {
        resetCountError();
    }
    try {
        return parseInt(totalErrorRequestString);
    } catch (e) {
        return 0;
    }
}

export function resetCountError() {
    localStorage.setItem('total_error_request', '0');
}

export function increaseCountErrorRequest(count = 1) {
    let currentTotalErrorRequest = getCountErrorRequest();
    localStorage.setItem('total_error_request', (currentTotalErrorRequest + count).toString());
}
