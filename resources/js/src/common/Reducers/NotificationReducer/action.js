export const PUSH_NOTIFICATION_ACTION = 'PUSH_NOTIFICATION_ACTION';
export const POP_NOTIFICATION_ACTION = 'POP_NOTIFICATION_ACTION';
export const RESET_NOTIFICATION_ACTION = 'RESET_NOTIFICATION_ACTION';
export const SKIP_NOTIFICATION_ACTION = 'SKIP_NOTIFICATION_ACTION';

/*
 * action creators
 */


export function resetNotificationAction() {
    return {type: RESET_NOTIFICATION_ACTION}
}

export function pushNotificationAction(response) {
    return {type: PUSH_NOTIFICATION_ACTION, payload: response}

}

export function popNotificationAction() {
    return {type: POP_NOTIFICATION_ACTION}
}
