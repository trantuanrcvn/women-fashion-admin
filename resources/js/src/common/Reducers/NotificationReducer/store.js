const varArrayNotification = 'array_notification';
const initItemNotification = {
    status: null,
    statusText: '',
    message: '',
    type: ''
}

export function getTotalNotification() {
    let arrayNotification = getArrayNotification();
    return arrayNotification.length;
}

export function getArrayNotification() {
    let notificationString = sessionStorage.getItem(varArrayNotification);
    if (notificationString === null) {
        resetArrayNotification();
    }
    try {
        if (notificationString === '') {
            return [];
        }
        return notificationString.split('#');
    } catch (e) {
        resetArrayNotification();
        return [];
    }
}

export function resetArrayNotification() {
    sessionStorage.setItem(varArrayNotification, '');
}

export function pushNotification(notification) {
    let status = notification.status;
    let statusText = notification.statusText;

    let dataResponse = notification.data !== undefined ? notification.data : {};
    let message = dataResponse.message !== undefined ? dataResponse.message : statusText;

    let type;
    if (100 <= status && status < 200) {
        type = 'info';
    } else if (200 <= status && status < 300) {
        type = 'success';
    } else if (300 <= status && status < 400) {
        type = 'info';
    } else if (400 <= status && status < 500) {
        type = 'error';
    } else if (500 <= status && status < 600) {
        message = statusText;
        type = 'error';
    } else {
        type = '';
    }

    switch (status) {
        case 204:
            message = 'Success';
            break;
        default:
            break;
    }

    let itemNotification = {
        status: status,
        statusText: statusText,
        message: message,
        type: type
    }
    let itemNotificationString = JSON.stringify(itemNotification);
    try {
        let arrayNotification = getArrayNotification();
        arrayNotification.push(itemNotificationString);
        let stringArrayNotification = arrayToString(arrayNotification, '#');
        sessionStorage.setItem(varArrayNotification, stringArrayNotification);
    } catch (e) {
        resetArrayNotification();
    }
}

export function popNotification() {

    try {
        let arrayNotification = getArrayNotification();
        let itemNotificationString = arrayNotification.shift();

        //Convert to string & update to storage
        let stringArrayNotification = arrayToString(arrayNotification, '#');
        sessionStorage.setItem(varArrayNotification, stringArrayNotification);

        //Check item notification
        if (itemNotificationString === undefined) {
            return initItemNotification;
        }

        //Return
        return JSON.parse(itemNotificationString);
    } catch (e) {
        //Reset array
        resetArrayNotification();

        //Return
        return initItemNotification
    }
}

export function arrayToString(array = [], charset = '#') {
    let string = '';
    for (let i = 0; i < array.length; i++) {
        if (i === array.length - 1) {
            string += array[i];
        } else {
            string += array[i] + charset;
        }
    }
    return string;
}
