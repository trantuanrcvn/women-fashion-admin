export const notificationStore = require('./store');
export const notificationAction = require('./action');

function initState() {
    return {
        currentNotification: notificationStore.popNotification(),
    }
}

export function notificationReducer(state = initState(), action) {
    switch (action.type) {
        case notificationAction.RESET_NOTIFICATION_ACTION:
            notificationStore.resetArrayNotification()
            return initState();
        case notificationAction.PUSH_NOTIFICATION_ACTION:
            notificationStore.pushNotification(action.payload);
            return initState();
        case notificationAction.POP_NOTIFICATION_ACTION:
            return initState();
        default:
            return state;
    }
}
