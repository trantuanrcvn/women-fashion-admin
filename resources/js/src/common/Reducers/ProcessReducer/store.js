const varArrayProcesses = 'array_processes';

export function getTotalProcess() {
    let arrayProcesses = getProcesses();
    return arrayProcesses.length;
}

export function getProcesses() {
    let processesString = sessionStorage.getItem(varArrayProcesses);
    if (processesString === null) {
        resetProcess();
    }
    try {
        if (processesString === '') {
            return [];
        }
        return processesString.split(',');
    } catch (e) {
        return [];
    }
}

export function resetProcess() {
    sessionStorage.setItem(varArrayProcesses, '');
}

export function pushProcesses(processes) {
    let arrayProcesses = getProcesses();
    arrayProcesses.push(processes);
    sessionStorage.setItem(varArrayProcesses, arrayProcesses.toString());
}

export function popProcesses(processes) {
    let arrayProcesses = getProcesses();
    for (let i = 0; i < arrayProcesses.length; i++) {
        if (arrayProcesses[i] === processes) {
            arrayProcesses.splice(i, 1);
            break;
        }
    }
    sessionStorage.setItem(varArrayProcesses, arrayProcesses.toString());
}
