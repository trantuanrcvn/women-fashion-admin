import {combineReducers} from 'redux';

export const processStore = require('./store');
export const processAction = require('./action');

function initState() {
    return {
        totalProcess: processStore.getTotalProcess(),
        arrayProcess: processStore.getProcesses()
    }
}

export function processReducer(state = initState(), action) {
    switch (action.type) {
        case processAction.RESET_PROCESS_ACTION:
            processStore.resetProcess()
            return initState();
        case processAction.PUSH_PROCESS_ACTION:
            processStore.pushProcesses(action.payload);
            return initState();
        case processAction.POP_PROCESS_ACTION:
            processStore.popProcesses(action.payload);
            return initState();
        default:
            return state;
    }
}

/*const authReducer = combineReducers({
    authentication,
})*/

/*export default authReducer*/
