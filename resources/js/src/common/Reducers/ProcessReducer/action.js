import {processAction} from "./index";
import axios from "axios";

export const PUSH_PROCESS_ACTION = 'PUSH_PROCESS_ACTION';
export const POP_PROCESS_ACTION = 'POP_PROCESS_ACTION';
export const RESET_PROCESS_ACTION = 'RESET_PROCESS_ACTION';

/*
 * action creators
 */


export function resetProcessAction() {
    return {type: RESET_PROCESS_ACTION}
}

export function pushProcessAction(processes) {
    return {type: PUSH_PROCESS_ACTION, payload: processes}
}

export function popProcessAction(processes) {
    return {type: POP_PROCESS_ACTION, payload: processes}
}
