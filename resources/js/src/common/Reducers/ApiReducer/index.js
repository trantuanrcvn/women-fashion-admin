export const apiAction = require('./action');

let initState = {
    'dataResponse': {},
    'status': null,
}

export function apiReducer(state = initState, action) {
    switch (action.type) {
        case apiAction.GET_ACTION:
            return {
                ...initState,
                status: action.payload.status,
                dataResponse: action.payload.dataResponse,
            };
        case apiAction.POST_ACTION:
            return {
                ...initState,
                status: action.payload.status,
                dataResponse: action.payload.dataResponse,
            };
        case apiAction.PUT_ACTION:
            return {
                ...initState,
                status: action.payload.status,
                dataResponse: action.payload.dataResponse,
            };
        case apiAction.DELETE_ACTION:
            return {
                ...initState,
                status: action.payload.status
            };
        default:
            return state;
    }
}
