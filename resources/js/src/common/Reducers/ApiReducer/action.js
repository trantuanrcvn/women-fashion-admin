import axios from "axios";
import {authStore} from '../AuthReducer';
import {processAction} from '../ProcessReducer';
import {errorAction} from '../ErrorReducer';
import {notificationAction} from "../NotificationReducer";

/*
 * action types
 */

export const GET_ACTION = 'GET_ACTION';
export const POST_ACTION = 'POST_ACTION';
export const PUT_ACTION = 'PUT_ACTION';
export const DELETE_ACTION = 'DELETE_ACTION';

/*
 * action creators
 */

export function getAction(status, dataResponse) {
    return {
        type: GET_ACTION,
        payload: {
            status, dataResponse
        }
    }
}

export function getData(url, params = {}, process = 'getData') {
    let config = authStore.getConfigHeader(params);
    return dispatch => {
        dispatch(processAction.pushProcessAction(process));
        return axios.get(url, config)
            .then(response => {
                thenFunction(dispatch, response, GET_ACTION);
            }).catch(error => {
                errorAction.catchFunction(dispatch, error)
            }).finally(() => {
                errorAction.finallyFunction(dispatch, process)
            });

    }
}

export function postAction(status, dataResponse) {
    return {
        type: POST_ACTION,
        payload: {
            status, dataResponse
        }
    }
}

export function postData(url, params, process = 'postData') {
    let config = authStore.getConfigHeader();
    return dispatch => {
        errorAction.beginFunction(dispatch, process);
        return axios.post(url, params, config)
            .then(response => {
                thenFunction(dispatch, response, POST_ACTION);
            }).catch(error => {
                errorAction.catchFunction(dispatch, error)
            }).finally(() => {
                errorAction.finallyFunction(dispatch, process)
            });
    }
}

export function putAction(status, dataResponse) {
    return {
        type: PUT_ACTION,
        payload: {
            status, dataResponse
        }
    }
}

export function putData(url, params, process = 'putData') {
    let config = authStore.getConfigHeader();
    return dispatch => {
        errorAction.beginFunction(dispatch, process);
        return axios.put(url, params, config)
            .then(response => {
                thenFunction(dispatch, response, PUT_ACTION);
            }).catch(error => {
                errorAction.catchFunction(dispatch, error)
            }).finally(() => {
                errorAction.finallyFunction(dispatch, process)
            });
    }
}

export function deleteAction(status, dataResponse) {
    return {
        type: DELETE_ACTION,
        payload: {
            status, dataResponse
        }
    }
}

export function deleteData(url, process = 'deleteData') {
    let config = authStore.getConfigHeader();
    return dispatch => {
        errorAction.beginFunction(dispatch, process);
        return axios.delete(url, config)
            .then(response => {
                thenFunction(dispatch, response, DELETE_ACTION);
            }).catch(error => {
                errorAction.catchFunction(dispatch, error)
            }).finally(() => {
                errorAction.finallyFunction(dispatch, process)
            });
    }
}

function thenFunction(dispatch, response, type) {
    switch (type) {
        case GET_ACTION:
            dispatch(getAction(response.status, response.data));
            break;
        case POST_ACTION:
            dispatch(postAction(response.status, response.data));
            break;
        case PUT_ACTION:
            dispatch(putAction(response.status, response.data));
            break;
        case DELETE_ACTION:
            dispatch(deleteAction(response.status, response.data));
            break;
        default:
            break;
    }

    if (type !== GET_ACTION) {
        dispatch(notificationAction.pushNotificationAction(response));
    }
}
