import {combineReducers} from 'redux';

import {authReducer} from "./AuthReducer";
import {apiReducer} from "./ApiReducer";
import {processReducer} from "./ProcessReducer";
import {notificationReducer} from "./NotificationReducer";
import {errorReducer} from "./ErrorReducer";

const reducers = combineReducers({
    apiReducer,
    authReducer,
    errorReducer,
    processReducer,
    notificationReducer,
})

export default reducers
